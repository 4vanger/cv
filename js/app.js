// store some app-wide properties
var AppModel = Backbone.Model.extend({
	defaults: {
		activeTab: 'home',
		lastScreen: null
	},
	isJobsLoaded: false,
	jobs: null,
	isProjectsLoaded: false,
	projects: null,
	initialize: function(){
		this.bind('change:lastScreen', this.changeLastScreen, this);
		this.jobs = new JobsCollection();
		this.jobs.once('reset', function(){this.isJobsLoaded = true}, this).fetch();
		this.projects = new ProjectsCollection();
		this.projects.once('reset', function(){this.isProjectsLoaded = true}, this).fetch();
	},
	whenJobsLoaded: function(callback, context){
		if(this.isJobsLoaded){
			// call immediately
			callback.apply(context);
		} else {
			this.jobs.once('reset', callback, context);
		}
	},
	whenProjectsLoaded: function(callback, context){
		if(this.isProjectsLoaded){
			// call immediately
			callback.apply(context);
		} else {
			this.projects.once('reset', callback, context);
		}
	},
	changeLastScreen: function(){
		var prev = this.previous('lastScreen');
		var newScreen = this.get('lastScreen');
		if(prev && newScreen != prev){
			prev.hide();
		}
	},
	setActiveTab: function(activeTab){
		this.set({activeTab: activeTab});
	},
	removeActiveTab: function(){
		this.unset('activeTab');
	}
});

var AppRouter = Backbone.Router.extend({
	routes: {
		"": "index",
		"!/": "index",
		'!/jobs': 'jobs',
		'!/projects': 'projects',
		'!/projects/:type': 'projectsByType',
		'!/contacts': 'contacts',
		'!/skill/:skill': 'skill'
	},
	initialize: function(options) {
		window.appRouter = this;
		this.app = new AppModel();
		new TopbarView();
		Backbone.history.start();
		return this;
	},
	pages: {
		index: {
			view: IndexView,
			tab: '.index'
		},
		jobs: {
			view: JobsView,
			tab: '.jobs'
		},
		projects: {
			view: ProjectsView,
			tab: '.projects'
		},
		contacts: {
			view: ContactsView,
			tab: '.contacts'
		}
	},
	showPage: function(pageType){
		var page = this.pages[pageType];
		if(page == null){
			alert('unknown page');
			return;
		}
		var view = page.viewObj || new page.view();
		page.viewObj = view;
		view.show();
		this.app.setActiveTab(page.tab);
	},
	index: function() {
		this.showPage('index');
	},
	jobs: function() {
		this.showPage('jobs');
	},
	projects: function() {
		this.showPage('projects');
	},
	projectsByType: function(typ){
		var view = new ProjectsView({'type': typ});
		view.show();
		this.app.setActiveTab(this.pages.projects.tab);
	},
	contacts: function() {
		this.showPage('contacts');
	},
	skill: function(skill){
		if(!this.app.isJobsLoaded){
			this.app.whenJobsLoaded(function(){this.skill(skill)}, this);
			return;
		}
		if(!this.app.isProjectsLoaded){
			this.app.whenProjectsLoaded(function(){this.skill(skill)}, this);
			return;
		}
		var view = new SkillsView({skill: skill});
		view.show();
	}
});

var Helper = Backbone.Model.extend({
	MONTHS: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
	getHRFDate: function(timestamp){
		var date = new Date(timestamp);
		return this.MONTHS[date.getMonth()] + ' ' + date.getFullYear()
	},
	getTimeSpan: function(startTimestamp, finishTimestamp){
		var start = new Date(startTimestamp);
		var finish = finishTimestamp != null ? new Date(finishTimestamp) : new Date();
		var monthsDiff = finish.getFullYear() * 12 + finish.getMonth() - start.getFullYear() * 12 - start.getMonth();
		var years = Math.floor(monthsDiff/12);
		var months = monthsDiff - years * 12;
		return 	(years != 0 ? years + ' year' + (years == 1 ? '' : 's') : '') + (years != 0 ? ' ' : '') +
				(months != 0 ? months + ' month' + (months == 1 ? '' : 's') : '');
	},
	getSkills: function(){
		return this.get('skills').split(/\s*,\s*/);
	}
});

var JobModel = Helper.extend({
	getStartHRF: function(){
		return this.getHRFDate(this.get('startTimestamp'));
	},
	getFinishHRF: function(){
		return this.has('finishTimestamp') ? this.getHRFDate(this.get('finishTimestamp')) : 'current';
	},
	getTimeSpanHRF: function(){
		return this.getTimeSpan(this.get('startTimestamp'), this.get('finishTimestamp'));
	}
});

var JobsCollection = Backbone.Collection.extend({
	urlRoot: 'data/jobs',
	url: "data/jobs.json",
	model: JobModel,
	comparator: function(job){
		return job.startTimestamp;
	}
});

var ProjectModel = Helper.extend({
	getStartHRF: function(){
		return this.getHRFDate(this.get('startTimestamp'));
	},
	getFinishHRF: function(){
		return this.has('finishTimestamp') ? this.getHRFDate(this.get('finishTimestamp')) : 'current';
	},
	getTimeSpanHRF: function(){
		return this.getTimeSpan(this.get('startTimestamp'), this.get('finishTimestamp'));
	}
});
var ProjectsCollection = Backbone.Collection.extend({
	urlRoot: 'data/projects',
	url: "data/projects.json",
	model: ProjectModel,
	comparator: function(project){
		return project.has('startTimestamp') ? -project.get('startTimestamp') : -Infinity;
	},
	getProjectsByType: function(typ){
		return this.filter(function(project){return project.get('type') === typ;})
	}
});

// start magic on page load
$(function () {
	new AppRouter();
});

// track analytics only for production
if(document.location.host.indexOf("4vanger.com") >= 0){
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-149419-2']);
	_gaq.push(['_setDomainName', '.4vanger.com']);
	_gaq.push(['_trackPageview']);

	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
}
