// Top bar visualization. The same for all pages but need to reflect current app state
var TopbarView = Backbone.View.extend({
	el: $('#topbar'),
	templates: {
		main: $("#topBarTemplate").template()
	},
	initialize: function(){
		// when tab changes - need to reflect that on page
		window.appRouter.app.bind('change:activeTab', this.changeTab, this);
		this.render();
	},
	render: function() {
		$.tmpl(this.templates.main, {}).appendTo(this.el);
		return this;
	},
	changeTab: function(){
		this.$('.nav .item').removeClass('active');
		var activeTab = window.appRouter.app.get('activeTab');
		if(activeTab){
			$('.topbar .nav .item' + activeTab).addClass('active')
		}
	}
});

var Screen = Backbone.View.extend({
	initialize: function(args){
		this.el = $(this.el);
		this.init(args);
	},
	init: function(){
		this.render();
	},
	show: function(){
		var app = window.appRouter.app;
		this.el.appendTo("#content").show();
		app.set({lastScreen: this});
	},
	hide: function(){
		var app = window.appRouter.app;
		this.el.slideUp(300, function(){$(this).remove()});
		if(app.get('lastScreen') == this){
			app.unset('lastScreen');
		}
	}
});

var Block = Backbone.View.extend({
	initialize: function(){
		this.el = $(this.el);
		this.render();
	},
	render: function() {
		$.tmpl(this.templates.main, {
			model: this.model,
			data: this.model.toJSON()
		}).appendTo(this.el);
		return this;
	}
});

var IndexView = Screen.extend({
	className: 'indexPage',
	templates: {
		main: $("#indexTemplate").template()
	},
	render: function() {
		$.tmpl(this.templates.main, {}).appendTo(this.el);
		return this;
	}
});

var JobsView = Screen.extend({
	className: 'jobsPage',
	templates: {
		main: $("#jobsTemplate").template()
	},
	init: function(){
		window.appRouter.app.whenJobsLoaded(this.render, this);
	},
	render: function() {
		$.tmpl(this.templates.main, {}).appendTo(this.el);
		var list = this.$('.jobsList');
		window.appRouter.app.jobs.each(function(job){
			var view = new JobView({model: job});
			view.el.appendTo(list);
		});
		return this;
	}
});

var JobView = Block.extend({
	className: 'job well',
	tagName: 'li',
	templates: {
		main: $("#jobTemplate").template()
	}
});

var ProjectsView = Screen.extend({
	className: 'projectsPage',
	templates: {
		main: $("#projectsTemplate").template()
	},
	init: function(args){
		this.typ = args != null ? args.type : null;
		window.appRouter.app.whenProjectsLoaded(this.render, this);
	},
	render: function() {
		var projects;
		if(this.typ == null){
			projects = window.appRouter.app.projects.toArray();
		} else {
			projects = window.appRouter.app.projects.getProjectsByType(this.typ);
		}
		$.tmpl(this.templates.main, {type: this.typ}).appendTo(this.el.empty());
		var list = this.$('.projectsList');
		_(projects).each(function(project){
			var view = new ProjectView({model: project});
			view.el.appendTo(list);
		});
		return this;
	},
	showProjectsByType: function(type){
		this.render();
	}
});

var ProjectView = Block.extend({
	className: 'job well',
	tagName: 'li',
	templates: {
		main: $("#projectTemplate").template()
	}
});

var ContactsView = Screen.extend({
	className: 'contactsPage hero-unit',
	templates: {
		main: $("#contactsTemplate").template()
	},
	render: function() {
		$.tmpl(this.templates.main, {}).appendTo(this.el);
		return this;
	}
});

var SkillsView = Screen.extend({
	className: 'skillsPage',
	templates: {
		main: $('#skillsTemplate').template()
	},
	init: function(args){
		this.skill = args.skill;
		this.render();
	},
	render: function(){
		$.tmpl(this.templates.main, {skill: this.skill}).appendTo(this.el.empty());
		var skill = this.skill;
		var jobs = window.appRouter.app.jobs.select(function(job){
			return _(job.getSkills()).detect(function(jobSkill){return jobSkill == skill;});
		});
		var projects = window.appRouter.app.projects.select(function(project){
			return _(project.getSkills()).detect(function(projectSkill){return projectSkill == skill;});
		});
		var obj = this;
		if(jobs.length > 0){
			obj.$('.jobs').removeClass('hidden');
			_(jobs).each(function(job){
				obj.$('.jobs ul').append((new JobView({model: job})).el);
			})
		}
		if(projects.length > 0){
			obj.$('.projects').removeClass('hidden');
			_(projects).each(function(project){
				obj.$('.projects ul').append((new ProjectView({model: project})).el);
			})
		}
	}
});
